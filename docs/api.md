##API Design

###How do you want you users to use your framework?

> Register Listeners: The framework should provide several events for users to register their own listeners.

> Several Scopes: The framework should provide several attribute-scopes for user to manipulate:
>   Page Scope, Request Scope, Session Scope, Application Scope and additional ValueStack.
>   The taste of this design is pretty much like struts2.

> Interceptor: Interceptors can consist Interceptor Stack. Different Actions can have different Interceptor Stack. Much like
>   the MiddleWare from Express and Connect. User can define their own MiddleWare and we provide some compulsory plug-in MiddleWares
>   for them to user.

> Action: Action represent a route. When the url address the web user applies can be mapped to specific Action, the action then is triggered.
>   Therefore, there should be some ActionMapper Utility to tell which Action Should be mapped according to the coming address.

> Result: Result has types, different result-type has different representational styles. For instance:
    Dispatcher(send params to page renders, usually a template engine),
        ejs;
        jade;
        mustache;
    Redirect(status code:304, tell the web browser to redirect to a given url),
        redirect url;
        redirect action;
    JSON(transform to JavaScript Object Strings),
    Chain(Chain to a given Action without send the response back),
    Stream(Binary Object),
    PlainText,
    xslt

    Result also has a lot of names: For instance:
        success,
        none,
        error,
        input,
        login


> Logger System: logger the activity of the framework