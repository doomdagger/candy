candy
=====

a brilliant  web framework for nodejs

## Installation

    $ npm install -g candy

## Quick Start

 The quickest way to get started with candy is to utilize the executable `candy(1)` to generate an application as shown below:

 Create the app:

    $ npm install -g candy
    $ candy /tmp/foo && cd /tmp/foo

 Install dependencies:

    $ npm install

 Start the server:

    $ node app

## Features

  * Built on [Connect](http://github.com/senchalabs/connect)
  * Robust routing
  * HTTP helpers (redirection, caching, etc)
  * View system supporting 14+ template engines
  * Content negotiation
  * Focus on high performance
  * Environment based configuration
  * Executable for generating applications quickly
  * High test coverage
