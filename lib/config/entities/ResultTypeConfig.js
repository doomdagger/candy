/**
 * Created by Li He on 2/26/14.
 */


module.exports = ResultTypeConfig;

/**
 *
 * @constructor
 */
function ResultTypeConfig(config){
    this.moduleName = config.moduleName||"";
    this.name = config.name||"";
    this.defaultResultParam = config.defaultResultParam||"";
    this.params = config.params||{};

}