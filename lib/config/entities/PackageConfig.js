/**
 * Created by Li He on 2/26/14.
 */

var _ = require("underscore"),
    utils = require("../../utils");

module.exports = PackageConfig;

/**
 *
 * @param config
 * @constructor
 */
function PackageConfig(config){
    this.actionConfigs = config.actionConfigs||{}; // map string,ActionConfig
    this.globalResultConfigs = config.globalResultConfigs||{}; // map string,ResultConfig
    this.interceptorConfigs = config.interceptorConfigs||{}; // map string,Object(interceptorConfig|interceptorStackConfig)
    this.resultTypeConfigs = config.resultTypeConfigs||{}; // map string,ResultConfig
    this.globalExceptionMappingConfigs = config.globalExceptionMappingConfigs||[]; // list
    this.parents = config.parents||[]; // list PackageConfig
    this.defaultInterceptorRef = config.defaultInterceptorRef||"";
    this.defaultActionRef = config.defaultActionRef||"";
    this.defaultResultType = config.defaultResultType||"";
    this.defaultClassRef = config.defaultClassRef||"";
    this.name = config.name||"";
    this.namespace = config.namespace||"";
    this.isAbstract = config.isAbstract||false;
    this.needsRefresh = config.needsRefresh||false;
}

/**
 * returns the Map of all the ActionConfigs available in the current package.
 * ActionConfigs defined in ancestor packages will be included in this Map.
 *
 * @return {} a Map of ActionConfig Objects with the action name as the key
 * @see ActionConfig
 */
PackageConfig.prototype.getAllActionConfigs = function(){
    var self = this;
    var parents = self.parents,
        retMap = {};

    if(!_.isEmpty(parents)){
        for(var parent in parents){
            utils.mixin(retMap, parent.getAllActionConfigs());
        }
    }
    utils.mixin(retMap, self.actionConfigs);

    return retMap;
};

/**
 * returns the Map of all the global ResultConfigs available in the current package.
 * Global ResultConfigs defined in ancestor packages will be included in this Map.
 *
 * @return {} a Map of Result Objects with the result name as the key
 * @see ResultConfig
 */
PackageConfig.prototype.getAllGlobalResults = function(){
    var self = this;
    var parents = self.parents,
        retMap = {};

    if(!_.isEmpty(parents)){
        for(var parent in parents){
            utils.mixin(retMap, parent.getAllGlobalResults());
        }
    }
    utils.mixin(retMap, self.globalResultConfigs);

    return retMap;
};

/**
 * returns the Map of all InterceptorConfigs and InterceptorStackConfigs available in the current package.
 * InterceptorConfigs defined in ancestor packages will be included in this Map.
 *
 * @return {} Map of InterceptorConfig and InterceptorStackConfig Objects with the ref-name as the key
 * @see InterceptorConfig
 * @see InterceptorStackConfig
 */
PackageConfig.prototype.getAllInterceptorConfigs = function(){
    var self = this;
    var parents = self.parents,
        retMap = {};

    if(!_.isEmpty(parents)){
        for(var parent in parents){
            utils.mixin(retMap, parent.getAllInterceptorConfigs());
        }
    }
    utils.mixin(retMap, self.interceptorConfigs);

    return retMap;
};


/**
 * returns the Map of all the ResultTypeConfigs available in the current package.
 * ResultTypeConfigs defined in ancestor packages will be included in this Map.
 *
 * @return {} a Map of ResultTypeConfig Objects with the result type name as the key
 * @see ResultTypeConfig
 */
PackageConfig.prototype.getAllResultTypeConfigs = function(){
    var self = this;
    var parents = self.parents,
        retMap = {};

    if(!_.isEmpty(parents)){
        for(var parent in parents){
            utils.mixin(retMap, parent.getAllResultTypeConfigs());
        }
    }
    utils.mixin(retMap, self.resultTypeConfigs);

    return retMap;
};

/**
 * returns the List of all the ExceptionMappingConfigs available in the current package.
 * ExceptionMappingConfigs defined in ancestor packages will be included in this list.
 *
 * @return {} a List of ExceptionMappingConfigs Objects with the result type name as the key
 * @see ExceptionMappingConfig
 */
PackageConfig.prototype.getAllExceptionMappingConfigs = function(){
    var self = this;
    var parents = self.parents,
        retMap = {};

    if(!_.isEmpty(parents)){
        for(var parent in parents){
            utils.mixin(retMap, parent.getAllExceptionMappingConfigs());
        }
    }
    utils.mixin(retMap, self.exceptionMappingConfigs);

    return retMap;
};

/**
 * When no class-ref noted for an action, candy will look up for this default ClassRef.
 * Configs in ancestor will be checked either
 * @returns {*}
 */
PackageConfig.prototype.getDefaultClassRef = function(){
    var defaultClassRef = this.defaultClassRef,
        parents = this.parents;
    if(!defaultClassRef && !_.isEmpty(parents)){
        for(var parent in parents){
            var parentDefault = parent.getDefaultClassRef();
            if(parentDefault){
                return parentDefault;
            }
        }
    }
    return defaultClassRef;
};

/**
 * gets the default interceptor-ref name. If this is not set on this PackageConfig, it searches the parent
 * PackageConfigs in order until it finds one.
 * @returns {*}
 */
PackageConfig.prototype.getFullDefaultInterceptorRef = function(){
    var defaultInterceptorRef = this.defaultInterceptorRef,
        parents = this.parents;
    if(!defaultInterceptorRef && !_.isEmpty(parents)){
        for(var parent in parents){
            var parentDefault = parent.getFullDefaultInterceptorRef();
            if(parentDefault){
                return parentDefault;
            }
        }
    }
    return defaultInterceptorRef;
};

/**
 * gets the default action-ref name. If this is not set on this PackageConfig, it searches the parent
 * PackageConfigs in order until it finds one.
 * @returns {*}
 */
PackageConfig.prototype.getFullDefaultActionRef = function(){
    var defaultActionRef = this.defaultActionRef,
        parents = this.parents;
    if(!defaultActionRef && !_.isEmpty(parents)){
        for(var parent in parents){
            var parentDefault = parent.getFullDefaultActionRef();
            if(parentDefault){
                return parentDefault;
            }
        }
    }
    return defaultActionRef;
};

/**
 * Returns the default result type for this package.
 * <p/>
 * If there is no default result type, but this package has parents - we will try to
 * look up the default result type of a parent.
 * @returns {*}
 */
PackageConfig.prototype.getFullDefaultResultType = function(){
    var defaultResultType = this.defaultResultType,
        parents = this.parents;
    if(!defaultResultType && !_.isEmpty(parents)){
        for(var parent in parents){
            var parentDefault = parent.getFullDefaultResultType();
            if(parentDefault){
                return parentDefault;
            }
        }
    }
    return defaultResultType;
};

/**
 * Find the Interceptor Config in this package according to the given name.
 * @param name name of a Interceptor
 * @returns {*}
 */
PackageConfig.prototype.getInterceptorConfig = function(name){
    return this.getAllInterceptorConfigs()[name];
};