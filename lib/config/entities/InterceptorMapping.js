/**
 * Created by apple on 2/26/14.
 */

var Interceptor = require("../../interceptor/Interceptor");

module.exports = InterceptorMapping;


function InterceptorMapping(config){
    this.name = config.name||"";
    this.interceptor = config.interceptor||Object.create(Interceptor.prototype);


}