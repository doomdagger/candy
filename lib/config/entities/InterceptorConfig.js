/**
 * Created by Lihe on 14-2-26.
 */

module.exports = InterceptorConfig;

/**
 *
 * @param config {params:[{name:"", value:""}], moduleName:"", name: ""}
 * @constructor
 */
function InterceptorConfig(config){
    this.params = config.params||{};  // map string,string
    this.moduleName = config.moduleName||"";
    this.name = config.name||"";


}

