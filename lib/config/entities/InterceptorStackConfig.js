/**
 * Created by apple on 2/26/14.
 */

module.exports = InterceptorStackConfig;

/**
 *
 * @param config
 * @constructor
 */
function InterceptorStackConfig(config){
    this.interceptorMappings = config.interceptorMappings||[]; // list interceptorMapping
    this.name = config.name||"";
}