/**
 * Created by Li He on 2/26/14.
 */

module.exports = ResultConfig;

function ResultConfig(config){
    this.params = config.params||{};
    this.moduleName = config.moduleName||"";
    this.name = config.name||"";
}

