/**
 * Created by apple on 2/26/14.
 */

module.exports = ExceptionMappingConfig;

function ExceptionMappingConfig(config){
    this.name = config.name||"";
    this.exceptionClassName = config.exceptionClassName||"";
    this.result = config.result||"";
    this.params = config.params||{};  // map string,string


}