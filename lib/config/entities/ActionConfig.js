/**
 * Created by apple on 2/26/14.
 */

var _ = require("underscore");

module.exports = ActionConfig;

function ActionConfig(config){
    this._default_method = "execute";
    this._wild_card = "*";

    this.interceptorMappings = config.interceptorMappings||[]; // list InterceptorMapping
    this.params = config.params||{}; // map string,string
    this.resultConfigs = config.resultConfigs||{}; // map string,ResultConfig
    this.exceptionMappingConfigs = config.exceptionMappingConfigs||[]; // list exceptionMappingConfig
    this.moduleName = config.moduleName||"";
    this.methodName = config.methodName||"";
    this.packageName = config.packageName||"";
    this.name = config.name||"";
    this.allowedMethods = config.allowedMethods||[]; //string array(Set:do not allow duplicate strings)
}

/**
 * is the method allowed to execute?
 * @param method {string} methodName
 */
ActionConfig.prototype.isAllowedMethod = function(method){
    var self = this;
    if(self.allowedMethods.length==1&&self._wild_card==self.allowedMethods[0]){
        return true;
    }else{
        return method == (self.methodName?self.methodName:self._default_method) || _.contains(self.allowedMethods,method);
    }
}