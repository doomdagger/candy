/**
 * Created by Lihe on 2/26/14.
 */

module.exports = JsonConfigurationProvider;

/**
 * JsonConfigurationProvider
 * Looks in the classpath for an XML file, "candy.json" by default,
 * and uses it for the XWork configuration.
 * @param config
 * @constructor
 */
function JsonConfigurationProvider(config){
    this.configFileName = config.configFileName||"candy.json";
    this.errorIfMissing = config.errorIfMissing||true;

    this.includedFileNames = []; // string Set
    this.loadedFileUrls = []; // string Set
    this.throwExceptionOnDuplicateBeans = true;
    this.declaredPackages = {};   //map string element

    this.configObject = undefined; // loaded json

    this.configuration = undefined;
}

JsonConfigurationProvider.prototype.init = function(configuration){ //configuration object
    var self = this;
    self.configuration = configuration;
    self.includedFileNames = configuration.getLoadedFileNames();
    self.loadObject(self.configFileName);
};

JsonConfigurationProvider.prototype.loadObject = function(configFileName){
    try{

    }catch(err){

    }
    var self = this;
    self.loadedFileUrls = [];
    self.configObject = self.loadConfigurationFiles(configFileName, null);
};

JsonConfigurationProvider.prototype.loadConfigurationFiles = function(configFileName, element){

};

