/**
 * Created by apple on 2/26/14.
 */


exports.mixin = function(a, b){
    if (a && b) {
        for (var key in b) {
            a[key] = b[key];
        }
    }
    return a;
};
