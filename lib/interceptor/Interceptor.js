/**
 * Created by Li He on 2/26/14.
 */

module.exports = Interceptor;

function Interceptor(){

}

Interceptor.prototype.intercept = function(actionInvocation){

    return actionInvocation.invoke();
}

Interceptor.prototype.init = function(){

}

Interceptor.prototype.destroy = function(){

}